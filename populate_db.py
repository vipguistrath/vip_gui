import csv
from datetime import datetime
import os

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'vip_gui.settings')

import django

django.setup()

from uploads.models import Data, DataSet


def populate():
    rows = load_csv('gambia.csv')
    u = add_dataset('Gambia')
    u = add_dataset('Malawi')


def add_dataset(name):
    u = DataSet.objects.get_or_create(name=name)[0]
    return u


if __name__ == '__main__':
    print("Starting Upload population script...")
    populate()
