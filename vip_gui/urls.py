from django.conf.urls import patterns, include, url

from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns(
    '',
    url(r'^admin/', include(admin.site.urls)),
    url(r'^uploads/', include('uploads.urls', namespace='uploads')),
    url(r'^accounts/', include('registration.backends.simple.urls'))
)
