import datetime
import time

from django.db.models import Max, Min, Q
from django.shortcuts import get_object_or_404
from django.views.generic import FormView, ListView

from nvd3 import lineWithFocusChart

from uploads.forms import UploadForm
from uploads.models import Data, DataSet

# Battery level constants
BATTERY_MIN_THRESHOLD = 12 * 0.3
BATTERY_MAX_THRESHOLD = 12 * 1.3


class IndexView(ListView):
    """View for the main screen, which shows a list of uploaded files."""
    model = DataSet
    context_object_name = 'datasets'


class DataSetView(ListView):
    """Shows a list of individual data points from a file."""
    template_name = 'uploads/dataset_view.html'
    paginate_by = 50
    context_object_name = 'data'

    def get_queryset(self):
        """Returns a list of data for a given upload."""
        self.dataset = get_object_or_404(DataSet, id=self.kwargs['uid'])
        return Data.objects.filter(dataset=self.dataset)

    def get_context_data(self, **kwargs):
        """
        Adds a reference to the upload to the context passed to
        the template.
        """
        context = super(DataSetView, self).get_context_data(**kwargs)
        context['dataset'] = self.dataset
        return context


class FileUploadView(FormView):
    form_class = UploadForm
    template_name = 'uploads/upload_form.html'
    success_url = '../'

    def get_initial(self):
        initial = super(FileUploadView, self).get_initial()
        initial['dsid'] = self.kwargs['uid']
        return initial

    def form_valid(self, form):
        form.handle_upload()
        return super(FileUploadView, self).form_valid(form)


class BatteryLevelFaultView(DataSetView):
    """
    Displays a list of faults, currently any time the value is outside a specific
    range.
    """
    template_name = 'uploads/batterylevelfault_view.html'


    # Get data from database, but only the points where the battery voltage is outwith the desired range
    def get_queryset(self):
        """Returns a list of 'faulty' data points"""
        data = super(BatteryLevelFaultView, self).get_queryset()
        return data.filter(
            Q(battery_voltage__gt=BATTERY_MAX_THRESHOLD) |
            Q(battery_voltage__lt=BATTERY_MIN_THRESHOLD))


    # Main code function
    def get_context_data(self, **kwargs):
        """
        Gets the number of lower and upper bound faults, and adds these to the
        context passed to the template.
        """
        context = super(BatteryLevelFaultView, self).get_context_data(**kwargs)

        lwarn = 0               # No. of Lower warnings
        uwarn = 0               # No. of Upper warnings
        faults = 0              # No. of Faults
        last_logtime = None     # Define this variable. This will store the current date for specific data point

        # Get faulty data points
        warnings = self.get_queryset()
        for warning in warnings:
            # This checks if two warnings have occurred within an hour, If so it increments faults
            if last_logtime:
                delta = warning.logtime - last_logtime
                if delta < datetime.timedelta(hours=1):
                    faults += 1
            last_logtime = warning.logtime      # Redefine the last_logtim with the date of the current datapoint

            # Calculate Number of lower and upper warnings. Function "load_is_below" is defined in models.py
            if warning.load_is_below(BATTERY_MIN_THRESHOLD):
                 lwarn += 1
            else:
                 uwarn += 1

        # Put these in the context passed to html template
        context['lwarn'] = lwarn
        context['uwarn'] = uwarn
        context['faults'] = faults
        return context


class DepthDischargeView(DataSetView):
    template_name = 'uploads/depthdischarge_view.html'

    # Return Max voltage value
    def get_queryset_max(self):
        data = super(DepthDischargeView, self).get_queryset()
        return data.aggregate(Max('battery_voltage'))['battery_voltage__max']

    # Return Min voltage value
    def get_queryset_min(self):
        data = super(DepthDischargeView, self).get_queryset()
        return data.aggregate(Min('battery_voltage'))['battery_voltage__min']

    ####################################################################################

    ## Ideally the following process will pull in previous Average DOD.
    ## At the moment it is taking the previous average DOD to be 0.

    ## This may not be needed however. The upload process now appends data into a single database.
    ## So each time this code runs it will import the entire data.
    ## When this process was originally created it was envisioned to upload new data to a different file.

    ####################################################################################


    def get_context_data(self, **kwargs):
        context = super(DepthDischargeView, self).get_context_data(**kwargs)

        ##### Data Imported from previous uploads
        AvDOD = 0
        totaluploads = 0
        total = (AvDOD/100)*12 * totaluploads   # = zero for now.
        ####

        ####################################################

        ## Make prediction of lifetime based on DOD

        ####################################################

        preditcion = 0
        fault = 0
        totaluploads = totaluploads + 1


        min = self.get_queryset_min()       # Get min
        max = self.get_queryset_max()       # Get max
        total = total + float(min)

        AvDOD = 100 - ((total/totaluploads)/12 * 100)

        if AvDOD >= 80:
            prediction = 0
            fault = 1;
        if AvDOD >= 60 and AvDOD < 80:
            prediction = 400            ## Cycles required to reduce batter to under 50% capacity
        if AvDOD >= 40 and AvDOD < 60:
            prediction = 600
        if AvDOD > 30 and AvDOD < 40:
            prediction = 1200
        if AvDOD >= 0 and AvDOD <= 30:
            prediction = 1500

        # Pass Data to html file through context
        context['min'] = min
        context['max'] = max
        context['AvDOD'] = AvDOD

        context['prediction']=prediction
        if fault == 1:
            context['diagnosis'] = 'Battery is being discharged too much!!!'
        if fault == 0:
            context['diagnosis'] = 'Battery is Being Discharged to a safe level'




        ####################################################

        ## Categorise Charge/Discharge Cycel

        ####################################################

        Date = 0    ## This is te date of the last good charge discharge cycle. This will also be imported from the last upload

        ####

        new = 0
        Report = ''

        if max < 11:
            Report = Report + 'Battery was not charged fully.'

        if max > 11:
            if AvDOD >= 50:
                Date = now.logtime
                new = 1
                Report = Report + 'Depth of discharge exceeded 50%.'
            if AvDOD < 50:
                Report = Report + 'Battery was not discharged enough.'


        if new == 1:
            Report = 'Battery went throuhg a proper cycle today. '+Report
        if new == 0:
            Report = 'Battery did not go though a proper cycle today. '+Report

        context['date'] = Date
        context['report'] =Report



        ####################################################

        ## Current battery voltage capacity as a percentage of initial battery capacity(12 V)

        ####################################################
        totalcapacity = 0    ## Data saved from last upload

        ####

        capacity = ((totalcapacity*totaluploads)*12)/100

        capacity = capacity + float(max)

        totalcapacity = ((capacity/totaluploads)/12)*100

        context['capacity']= totalcapacity

        return context


class PowerFaultView(DataSetView):
    template_name = 'uploads/powerfault_view.html'

    def get_queryset(self):
        data = super(PowerFaultView, self).get_queryset()
        return data


    def get_context_data(self, **kwargs):
        context = super(PowerFaultView, self).get_context_data(**kwargs)


        D = self.get_queryset()     # Get data set. Unfortunately this imports all data fields so is not very efficient

        # Initialise "a few" variables

        D_total_power = 0

        M_total_power = 0
        Daily_Power = []
        D_dates = []
        Monthly_Power = []
        W_dates = []
        M_dates = []
        Y_dates = []

        Weekly_Power = []
        W_Total_Power = 0


        days = 0
        weeks = 0
        months = 0

        d_count = 0
        w_count = 0
        m_count = 0

        m_d = []


        # Define the last logtime as the logtime of the first element.
        last_logtime = D[0].logtime


        for i in D:
            if last_logtime:
                ## Get Average Daily Power
                if (i.logtime.date() == last_logtime.date()):       ## If the data point has the same day as the previous

                    # "get_gen_power" function is defined in "models.py"
                    D_total_power = D_total_power + i.get_gen_power()   ## Sum power to get the total in a day
                    d_count = d_count + 1                               ## Get number of data points

                else:
                    Daily_Power.append(D_total_power/d_count)                           # Divide by the number of data points to get the average instantanious power
                    D_dates.append(int(time.mktime(last_logtime.timetuple())) * 1000)   # Required Date format for graphs


                    # Same for weeks
                    if days % 7 != 0 or days ==0:
                        W_Total_Power = W_Total_Power + D_total_power
                        w_count = w_count + d_count

                    if days % 7 == 0 and days != 0:
                        Weekly_Power.append(W_Total_Power/w_count)              # Divide by the number of data points to get the average instantanious power
                        weeks = weeks + 1
                        W_dates.append(D_dates[days])
                        W_Total_Power = D_total_power
                        w_count = 0

                    days = days + 1         # Increment days to get the total number of days

                    D_total_power = i.get_gen_power()       # Re-define total power so as to start the power sum for the next day
                    d_count = 0

                ## Get Average Monthly Power
                if (i.logtime.month == last_logtime.month):
                    M_total_power = M_total_power + i.get_gen_power()
                    m_count = m_count + 1

                else:

                    Monthly_Power.append(M_total_power/m_count)
                    m_d.append(int(time.mktime(last_logtime.timetuple())) * 1000)   # Format for graph
                    M_dates.append(last_logtime.month)                              # Format for table in html file
                    Y_dates.append(last_logtime.year)                               # Format for table in html file

                    M_total_power = i.get_gen_power()

                    months = months + 1
                    m_count = 0

            last_logtime = i.logtime            # Reset last_logtime to the date of the current data point


    ####################################################

    ## Calculate the Average Power each day/week/month

    ####################################################


        T_D_Power = 0
        T_M_Power = 0
        T_W_Power = 0
        j = 0
        k = 0
        l = 0
        for j in range (days):
            if j == 0:
                T_D_Power = int(Daily_Power[int(j)])
            else:
                T_D_Power = T_D_Power + int(Daily_Power[int(j)])

        Total_Av_Daily_Power = int(T_D_Power)/days


        for l in range (int(weeks)):
            if l == 0:
                T_W_Power = int(Weekly_Power[int(l)])
            else:
                T_W_Power = T_W_Power + int(Weekly_Power[int(l)])

        Total_Av_Weekly_Power = int(T_W_Power)/weeks

        for k in range (months):
            if k == 0:
                T_M_Power = Monthly_Power[int(k)]
            else:
                T_M_Power = T_M_Power + Monthly_Power[int(k)]

        Total_Av_Monthly_Power = int(T_M_Power)/months


        ####################################################

        ## Calculate the No. above and below 115 % and 85 % of the average
        ## Save these into a list to display on a table

        ####################################################


        dbelow = 0
        dabove = 0
        wbelow = 0
        wabove = 0
        mbelow = 0
        mabove = 0

        list1 = []
        list2 = []
        list3 = []

        element = 0
        for element in range (days):

            if Daily_Power[element]  < (0.85*Total_Av_Daily_Power):
                dbelow = dbelow + 1
                day_date = {'date': D_dates[element], 'Daily_Power': Daily_Power[element]}
                list1.append(day_date)
            if Daily_Power[element]  > (1.15*Total_Av_Daily_Power):
                dabove = dabove + 1
                day_date = {'date': D_dates[element], 'Daily_Power': Daily_Power[element]}
                list1.append(day_date)



        element = 0
        for element in range (int(weeks)):

            if Weekly_Power[element] < (0.85*Total_Av_Weekly_Power):
                wbelow = wbelow + 1
                week_date = {'date': W_dates[element], 'Weekly_Power': Weekly_Power[element]}
                list2.append(week_date)
            if Weekly_Power[element]  > (1.15*Total_Av_Weekly_Power):
                wabove = wabove + 1
                week_date = {'date': W_dates[element], 'Weekly_Power': Weekly_Power[element]}
                list2.append(week_date)

        element = 0
        for element in range (months):

            if Monthly_Power[element]  < (0.85*Total_Av_Monthly_Power):
                mbelow = mbelow + 1
                month_date = {'date': M_dates[element], 'Year': Y_dates[element], 'Monthly_Power': Monthly_Power[element]}
                list3.append(month_date)
            if Monthly_Power[element]  > (1.15*Total_Av_Monthly_Power):
                mabove = mabove + 1
                month_date = {'date': M_dates[element], 'Year': Y_dates[element], 'Monthly_Power': Monthly_Power[element]}
                list3.append(month_date)




        ####################################################

        ## Create Graphs

        ####################################################


        Av_D_P = []
        Av_M_P = []
        Av_W_P = []

        U_T_D = []
        L_T_D = []

        U_T_W = []
        L_T_W = []

        U_T_M = []
        L_T_M = []


        for count in range (days):
            Av_D_P.append(int(Daily_Power[int(count)]))
            L_T_D.append(int(0.85*Total_Av_Daily_Power))        ## Define upper an lower thresholds as an array
            U_T_D.append(int(1.15*Total_Av_Daily_Power))        ## In order to represent them as a straight horizontal
                                                                ## line on the graph


        for count in range (weeks):
            Av_W_P.append(int(Weekly_Power[int(count)]))
            L_T_W.append(int(0.85*Total_Av_Weekly_Power))
            U_T_W.append(int(1.15*Total_Av_Weekly_Power))

        for count in range (months):
            Av_M_P.append(int(Monthly_Power[int(count)]))
            L_T_M.append(int(0.85*Total_Av_Monthly_Power))
            U_T_M.append(int(1.15*Total_Av_Monthly_Power))



        type = 'lineWithFocusChart'
        chart1 = lineWithFocusChart(name=type, x_is_date=True, x_axis_format="%d %b %Y")
        xdata = D_dates
        ydata = Av_D_P
        extra_serie = {"tooltip": {"y_start": "", "y_end": " V"},
                       "date_format": "%d %b %Y %H:%M:%S"}
        chart1.add_serie(y=ydata, x=xdata, name='Average Daily Instantanious Power(Watt)', extra=extra_serie)
        chart1.add_serie(y=U_T_D, x=xdata, name='Upper Threshhold', extra=extra_serie)
        chart1.add_serie(y=L_T_D, x=xdata, name='Lower Threshhold', extra=extra_serie)
        chart1.buildcontent()

        chart2 = lineWithFocusChart(name=type, x_is_date=True, x_axis_format="%d %b %Y")
        xdata2 = W_dates
        ydata2 = Av_W_P
        extra_serie = {"tooltip": {"y_start": "", "y_end": " V"},
                       "date_format": "%d %b %Y %H:%M:%S"}
        chart2.add_serie(y=ydata2, x=xdata2, name='Weekly Power(Watt)', extra=extra_serie)
        chart2.add_serie(y=U_T_W, x=xdata2, name='Upper Threshhold', extra=extra_serie)
        chart2.add_serie(y=L_T_W, x=xdata2, name='Lower Threshhold', extra=extra_serie)
        chart2.buildcontent()


        chart3 = lineWithFocusChart(name=type, x_is_date=True, x_axis_format="%d %b %Y")
        xdata3 = m_d
        ydata3 = Av_M_P
        extra_serie = {"tooltip": {"y_start": "", "y_end": " V"},
                       "date_format": "%d %b %Y %H:%M:%S"}
        chart3.add_serie(y=ydata3, x=xdata3, name='Monthly Power(Watt)', extra=extra_serie)
        chart3.add_serie(y=U_T_M, x=xdata3, name='Upper Threshhold', extra=extra_serie)
        chart3.add_serie(y=L_T_M, x=xdata3, name='Lower Threshhold', extra=extra_serie)
        chart3.buildcontent()






        context['chart1'] = chart1.htmlcontent
        context['chart2'] = chart2.htmlcontent
        context['chart3'] = chart3.htmlcontent


        context['D_av_power'] = Total_Av_Daily_Power
        context['W_av_power'] = Total_Av_Weekly_Power
        context['M_av_power'] = Total_Av_Monthly_Power

        context['dbelow'] = dbelow
        context['dabove'] = dabove
        context['wbelow'] = wbelow
        context['wabove'] = wabove
        context['mbelow'] = mbelow
        context['mabove'] = mabove

        context['table1'] = list1
        context['table2'] = list2
        context['table3'] = list3


        return context







####################################################

## Remaining fault classes are similar to the previous and do not contain the same level of comments

####################################################






class EnergyView(DataSetView):
    template_name = 'uploads/Energy_view.html'

    def get_queryset(self):
        data = super(EnergyView, self).get_queryset()
        return data


    def get_context_data(self, **kwargs):
        context = super(EnergyView, self).get_context_data(**kwargs)

        D = self.get_queryset()


        D_total_power = 0
        W_Total_Power = 0
        M_total_power = 0

        D_dates = []
        W_dates = []
        M_dates = []
        Y_dates = []

        days = 0
        weeks = 0
        months = 0

        d_count = 0
        w_count = 0
        m_count = 0


        D_Energy = 0
        W_Energy = 0
        M_Energy = 0

        D_En = 0
        W_En = 0
        M_En = 0

        m_d = []

        Daily_Energy = []
        Weekly_Energy = []
        Monthly_Energy = []



        last_logtime = D[0].logtime

        for i in D:
            if last_logtime:
                ## Get Average Daily Power
                if (i.logtime.date() == last_logtime.date()):

                    D_total_power = D_total_power + i.get_gen_power()
                    d_count = d_count + 1

                else:
                    D_En = D_total_power*24/(d_count)                  # Watt-Hour
                    D_dates.append(int(time.mktime(last_logtime.timetuple())) * 1000)

                    Daily_Energy.append(D_En)

                    if days % 7 != 0 or days ==0:
                        W_Total_Power = W_Total_Power + D_total_power
                        w_count = w_count + 1

                    if days % 7 == 0 and days != 0:

                        W_En = W_Total_Power*24*7/(w_count*d_count)                        # Watt-Hour
                        Weekly_Energy.append(W_En)
                        weeks = weeks + 1
                        W_dates.append(D_dates[days])
                        W_Total_Power = D_total_power
                        w_count = 0

                    D_total_power = i.get_gen_power()

                    days = days + 1
                    d_count = 0



                ## Get Average Monthly Energy
                if (i.logtime.month == last_logtime.month):

                    M_total_power = M_total_power + i.get_gen_power()
                    m_count = m_count + 1

                else:
                    M_En = M_total_power*24*7*4/(m_count)              # Watt-Hour
                    m_d.append(int(time.mktime(last_logtime.timetuple())) * 1000)

                    M_dates.append(last_logtime.month)
                    Y_dates.append(last_logtime.year)

                    M_total_power = i.get_gen_power()
                    months = months + 1
                    m_count = 0

                    Monthly_Energy.append(M_En)


            last_logtime = i.logtime

        T_D_Energy = 0
        T_M_Energy = 0
        T_W_Energy = 0
        j = 0
        k = 0
        l = 0


        for j in range (days):

            if j == 0:
                T_D_Energy = int(Daily_Energy[int(j)])
            else:
                T_D_Energy = T_D_Energy + int(Daily_Energy[int(j)])

        Total_Av_Daily_Energy = int(T_D_Energy)/days


        for l in range (weeks):

            if l == 0:
                T_W_Energy = int(Weekly_Energy[int(l)])
            else:
                T_W_Energy = T_W_Energy + int(Weekly_Energy[int(l)])

        Total_Av_Weekly_Energy = int(T_W_Energy)/weeks

        for k in range (months):
            if k == 0:
                T_M_Energy = Monthly_Energy[int(k)]
            else:
                T_M_Energy = T_M_Energy + Monthly_Energy[int(k)]

        Total_Av_Monthly_Energy = int(T_M_Energy)/months






        dbelow = 0
        dabove = 0
        wbelow = 0
        wabove = 0
        mbelow = 0
        mabove = 0

        list1 = []
        list2 = []
        list3 = []



        element = 0
        for element in range (days):

            if Daily_Energy[element] < 0.85*Total_Av_Daily_Energy:
                dbelow = dbelow + 1
                day_date = {'date': D_dates[element], 'Daily_Energy': Daily_Energy[element]}
                list1.append(day_date)
            if Daily_Energy[element] > 1.15*Total_Av_Daily_Energy:
                dabove = dabove + 1
                day_date = {'date': D_dates[element], 'Daily_Energy': Daily_Energy[element]}
                list1.append(day_date)

        element = 0
        for element in range (weeks):

            if Weekly_Energy[element] < 0.85*Total_Av_Weekly_Energy:
                wbelow = wbelow + 1
                day_date = {'date': W_dates[element], 'Average_Weekly_Energy': Weekly_Energy[element]}
                list2.append(day_date)
            if Weekly_Energy[element] > 1.15*Total_Av_Weekly_Energy:
                wabove = wabove + 1
                day_date = {'date': W_dates[element], 'Weekly_Energy': Weekly_Energy[element]}
                list2.append(day_date)

        element = 0
        for element in range (months):

            if Monthly_Energy[element] < 0.85*Total_Av_Monthly_Energy:
                mbelow = mbelow + 1
                month_date = {'date': M_dates[element], 'Year': Y_dates[element], 'Monthly_Energy': Monthly_Energy[element]}
                list3.append(month_date)
            if Monthly_Energy[element] > 1.15*Total_Av_Monthly_Energy:
                mabove = mabove + 1
                month_date = {'date': M_dates[element], 'Year': Y_dates[months-1], 'Monthly_Energy': Monthly_Energy[element]}
                list3.append(month_date)


        Av_D_E = []
        Av_M_E = []
        Av_W_E = []

        Av_D_E = []
        Av_M_E = []
        Av_W_E = []

        U_T_D = []
        L_T_D = []

        U_T_W = []
        L_T_W = []

        U_T_M = []
        L_T_M = []

        for count in range (days):
            Av_D_E.append(int(Daily_Energy[int(count)]))
            L_T_D.append(int(0.85*Total_Av_Daily_Energy))
            U_T_D.append(int(1.15*Total_Av_Daily_Energy))

        for count in range (weeks):
            Av_W_E.append(int(Weekly_Energy[int(count)]))
            L_T_W.append(int(0.85*Total_Av_Weekly_Energy))
            U_T_W.append(int(1.15*Total_Av_Weekly_Energy))

        for count in range (months):
            Av_M_E.append(int(Monthly_Energy[int(count)]))
            L_T_M.append(int(0.85*Total_Av_Monthly_Energy))
            U_T_M.append(int(1.15*Total_Av_Monthly_Energy))


        type = 'lineWithFocusChart'
        chart1 = lineWithFocusChart(name=type, x_is_date=True, x_axis_format="%d %b %Y")
        xdata = D_dates
        ydata = Av_D_E
        extra_serie = {"tooltip": {"y_start": "", "y_end": " V"},
                       "date_format": "%d %b %Y %H:%M:%S"}
        chart1.add_serie(y=ydata, x=xdata, name='Daily Energy(Watt-Hour)', extra=extra_serie)
        chart1.add_serie(y=U_T_D, x=xdata, name='Upper Threshhold', extra=extra_serie)
        chart1.add_serie(y=L_T_D, x=xdata, name='Lower Threshhold', extra=extra_serie)
        chart1.buildcontent()

        chart2 = lineWithFocusChart(name=type, x_is_date=True, x_axis_format="%d %b %Y")
        xdata2 = W_dates
        ydata2 = Av_W_E
        extra_serie = {"tooltip": {"y_start": "", "y_end": " V"},
                       "date_format": "%d %b %Y %H:%M:%S"}
        chart2.add_serie(y=ydata2, x=xdata2, name='Weekly Energy(Watt-Hour)', extra=extra_serie)
        chart2.add_serie(y=U_T_W, x=xdata2, name='Upper Threshhold', extra=extra_serie)
        chart2.add_serie(y=L_T_W, x=xdata2, name='Lower Threshhold', extra=extra_serie)
        chart2.buildcontent()


        chart3 = lineWithFocusChart(name=type, x_is_date=True, x_axis_format="%d %b %Y")
        xdata3 = m_d
        ydata3 = Av_M_E
        extra_serie = {"tooltip": {"y_start": "", "y_end": " V"},
                       "date_format": "%d %b %Y %H:%M:%S"}
        chart3.add_serie(y=ydata3, x=xdata3, name='Monthly Energy(Watt-Hour)', extra=extra_serie)
        chart3.add_serie(y=U_T_M, x=xdata3, name='Upper Threshhold', extra=extra_serie)
        chart3.add_serie(y=L_T_M, x=xdata3, name='Lower Threshhold', extra=extra_serie)
        chart3.buildcontent()



        context['chart1'] = chart1.htmlcontent
        context['chart2'] = chart2.htmlcontent
        context['chart3'] = chart3.htmlcontent



        context['D_av_energy'] = Total_Av_Daily_Energy
        context['W_av_energy'] = Total_Av_Weekly_Energy
        context['M_av_energy'] = Total_Av_Monthly_Energy

        context['dbelow'] = dbelow
        context['dabove'] = dabove
        context['wbelow'] = wbelow
        context['wabove'] = wabove
        context['mbelow'] = mbelow
        context['mabove'] = mabove

        context['table1'] = list1
        context['table2'] = list2
        context['table3'] = list3


        return context





class PowerLoadView(DataSetView):
    template_name = 'uploads/powerload_view.html'

    def get_queryset(self):
        data = super(PowerLoadView, self).get_queryset()
        return data


    def get_context_data(self, **kwargs):
        context = super(PowerLoadView, self).get_context_data(**kwargs)


        D = self.get_queryset()     # Get data set. Unfortunately this imports all data fields so is not very efficient

        # Initialise "a few" variables

        D_total_power = 0

        M_total_power = 0
        Daily_Power = []
        D_dates = []
        Monthly_Power = []
        W_dates = []
        M_dates = []
        Y_dates = []

        Weekly_Power = []
        W_Total_Power = 0


        days = 0
        weeks = 0
        months = 0

        d_count = 0
        w_count = 0
        m_count = 0

        m_d = []


        # Define the last logtime as the logtime of the first element.
        last_logtime = D[0].logtime


        for i in D:
            if last_logtime:
                ## Get Average Daily Power
                if (i.logtime.date() == last_logtime.date()):       ## If the data point has the same day as the previous

                    # "get_load_power" function is defined in "models.py"
                    D_total_power = D_total_power + i.get_load_power()   ## Sum power to get the total in a day
                    d_count = d_count + 1                               ## Get number of data points

                else:
                    Daily_Power.append(D_total_power/d_count)                           # Divide by the number of data points to get the average instantanious power
                    D_dates.append(int(time.mktime(last_logtime.timetuple())) * 1000)   # Required Date format for graphs


                    # Same for weeks
                    if days % 7 != 0 or days ==0:
                        W_Total_Power = W_Total_Power + D_total_power
                        w_count = w_count + d_count

                    if days % 7 == 0 and days != 0:
                        Weekly_Power.append(W_Total_Power/w_count)              # Divide by the number of data points to get the average instantanious power
                        weeks = weeks + 1
                        W_dates.append(D_dates[days])
                        W_Total_Power = D_total_power
                        w_count = 0

                    days = days + 1         # Increment days to get the total number of days

                    D_total_power = i.get_load_power()       # Re-define total power so as to start the power sum for the next day
                    d_count = 0

                ## Get Average Monthly Power
                if (i.logtime.month == last_logtime.month):
                    M_total_power = M_total_power + i.get_load_power()
                    m_count = m_count + 1

                else:

                    Monthly_Power.append(M_total_power/m_count)
                    m_d.append(int(time.mktime(last_logtime.timetuple())) * 1000)   # Format for graph
                    M_dates.append(last_logtime.month)                              # Format for table in html file
                    Y_dates.append(last_logtime.year)                               # Format for table in html file

                    M_total_power = i.get_load_power()

                    months = months + 1
                    m_count = 0

            last_logtime = i.logtime            # Reset last_logtime to the date of the current data point


    ####################################################

    ## Calculate the Average Power each day/week/month

    ####################################################


        T_D_Power = 0
        T_M_Power = 0
        T_W_Power = 0
        j = 0
        k = 0
        l = 0
        for j in range (days):
            if j == 0:
                T_D_Power = int(Daily_Power[int(j)])
            else:
                T_D_Power = T_D_Power + int(Daily_Power[int(j)])

        Total_Av_Daily_Power = int(T_D_Power)/days


        for l in range (int(weeks)):
            if l == 0:
                T_W_Power = int(Weekly_Power[int(l)])
            else:
                T_W_Power = T_W_Power + int(Weekly_Power[int(l)])

        Total_Av_Weekly_Power = int(T_W_Power)/weeks

        for k in range (months):
            if k == 0:
                T_M_Power = Monthly_Power[int(k)]
            else:
                T_M_Power = T_M_Power + Monthly_Power[int(k)]

        Total_Av_Monthly_Power = int(T_M_Power)/months


        ####################################################

        ## Calculate the No. above and below 115 % and 85 % of the average
        ## Save these into a list to display on a table

        ####################################################


        dbelow = 0
        dabove = 0
        wbelow = 0
        wabove = 0
        mbelow = 0
        mabove = 0

        list1 = []
        list2 = []
        list3 = []

        element = 0
        for element in range (days):

            if Daily_Power[element]  < (0.85*Total_Av_Daily_Power):
                dbelow = dbelow + 1
                day_date = {'date': D_dates[element], 'Daily_Power': Daily_Power[element]}
                list1.append(day_date)
            if Daily_Power[element]  > (1.15*Total_Av_Daily_Power):
                dabove = dabove + 1
                day_date = {'date': D_dates[element], 'Daily_Power': Daily_Power[element]}
                list1.append(day_date)



        element = 0
        for element in range (int(weeks)):

            if Weekly_Power[element] < (0.85*Total_Av_Weekly_Power):
                wbelow = wbelow + 1
                week_date = {'date': W_dates[element], 'Weekly_Power': Weekly_Power[element]}
                list2.append(week_date)
            if Weekly_Power[element]  > (1.15*Total_Av_Weekly_Power):
                wabove = wabove + 1
                week_date = {'date': W_dates[element], 'Weekly_Power': Weekly_Power[element]}
                list2.append(week_date)

        element = 0
        for element in range (months):

            if Monthly_Power[element]  < (0.85*Total_Av_Monthly_Power):
                mbelow = mbelow + 1
                month_date = {'date': M_dates[element], 'Year': Y_dates[element], 'Monthly_Power': Monthly_Power[element]}
                list3.append(month_date)
            if Monthly_Power[element]  > (1.15*Total_Av_Monthly_Power):
                mabove = mabove + 1
                month_date = {'date': M_dates[element], 'Year': Y_dates[element], 'Monthly_Power': Monthly_Power[element]}
                list3.append(month_date)




        ####################################################

        ## Create Graphs

        ####################################################


        Av_D_P = []
        Av_M_P = []
        Av_W_P = []

        U_T_D = []
        L_T_D = []

        U_T_W = []
        L_T_W = []

        U_T_M = []
        L_T_M = []


        for count in range (days):
            Av_D_P.append(int(Daily_Power[int(count)]))
            L_T_D.append(int(0.85*Total_Av_Daily_Power))        ## Define upper an lower thresholds as an array
            U_T_D.append(int(1.15*Total_Av_Daily_Power))        ## In order to represent them as a straight horizontal
                                                                ## line on the graph


        for count in range (weeks):
            Av_W_P.append(int(Weekly_Power[int(count)]))
            L_T_W.append(int(0.85*Total_Av_Weekly_Power))
            U_T_W.append(int(1.15*Total_Av_Weekly_Power))

        for count in range (months):
            Av_M_P.append(int(Monthly_Power[int(count)]))
            L_T_M.append(int(0.85*Total_Av_Monthly_Power))
            U_T_M.append(int(1.15*Total_Av_Monthly_Power))



        type = 'lineWithFocusChart'
        chart1 = lineWithFocusChart(name=type, x_is_date=True, x_axis_format="%d %b %Y")
        xdata = D_dates
        ydata = Av_D_P
        extra_serie = {"tooltip": {"y_start": "", "y_end": " V"},
                       "date_format": "%d %b %Y %H:%M:%S"}
        chart1.add_serie(y=ydata, x=xdata, name='Average Daily Instantanious Power(Watt)', extra=extra_serie)
        chart1.add_serie(y=U_T_D, x=xdata, name='Upper Threshhold', extra=extra_serie)
        chart1.add_serie(y=L_T_D, x=xdata, name='Lower Threshhold', extra=extra_serie)
        chart1.buildcontent()

        chart2 = lineWithFocusChart(name=type, x_is_date=True, x_axis_format="%d %b %Y")
        xdata2 = W_dates
        ydata2 = Av_W_P
        extra_serie = {"tooltip": {"y_start": "", "y_end": " V"},
                       "date_format": "%d %b %Y %H:%M:%S"}
        chart2.add_serie(y=ydata2, x=xdata2, name='Weekly Power(Watt)', extra=extra_serie)
        chart2.add_serie(y=U_T_W, x=xdata2, name='Upper Threshhold', extra=extra_serie)
        chart2.add_serie(y=L_T_W, x=xdata2, name='Lower Threshhold', extra=extra_serie)
        chart2.buildcontent()


        chart3 = lineWithFocusChart(name=type, x_is_date=True, x_axis_format="%d %b %Y")
        xdata3 = m_d
        ydata3 = Av_M_P
        extra_serie = {"tooltip": {"y_start": "", "y_end": " V"},
                       "date_format": "%d %b %Y %H:%M:%S"}
        chart3.add_serie(y=ydata3, x=xdata3, name='Monthly Power(Watt)', extra=extra_serie)
        chart3.add_serie(y=U_T_M, x=xdata3, name='Upper Threshhold', extra=extra_serie)
        chart3.add_serie(y=L_T_M, x=xdata3, name='Lower Threshhold', extra=extra_serie)
        chart3.buildcontent()






        context['chart1'] = chart1.htmlcontent
        context['chart2'] = chart2.htmlcontent
        context['chart3'] = chart3.htmlcontent


        context['D_av_power'] = Total_Av_Daily_Power
        context['W_av_power'] = Total_Av_Weekly_Power
        context['M_av_power'] = Total_Av_Monthly_Power

        context['dbelow'] = dbelow
        context['dabove'] = dabove
        context['wbelow'] = wbelow
        context['wabove'] = wabove
        context['mbelow'] = mbelow
        context['mabove'] = mabove

        context['table1'] = list1
        context['table2'] = list2
        context['table3'] = list3


        return context


class EnergyLoadView(DataSetView):
    template_name = 'uploads/Energyload_view.html'

    def get_queryset(self):
        data = super(EnergyLoadView, self).get_queryset()
        return data


    def get_context_data(self, **kwargs):
        context = super(EnergyLoadView, self).get_context_data(**kwargs)

        D = self.get_queryset()


        D_total_power = 0
        W_Total_Power = 0
        M_total_power = 0

        D_dates = []
        W_dates = []
        M_dates = []
        Y_dates = []

        days = 0
        weeks = 0
        months = 0

        d_count = 0
        w_count = 0
        m_count = 0


        D_Energy = 0
        W_Energy = 0
        M_Energy = 0

        D_En = 0
        W_En = 0
        M_En = 0

        m_d = []

        Daily_Energy = []
        Weekly_Energy = []
        Monthly_Energy = []



        last_logtime = D[0].logtime

        for i in D:
            if last_logtime:
                ## Get Average Daily Power
                if (i.logtime.date() == last_logtime.date()):

                    D_total_power = D_total_power + i.get_load_power()
                    d_count = d_count + 1

                else:
                    D_En = D_total_power*24/(d_count)                  # Watt-Hour
                    D_dates.append(int(time.mktime(last_logtime.timetuple())) * 1000)

                    Daily_Energy.append(D_En)

                    if days % 7 != 0 or days ==0:
                        W_Total_Power = W_Total_Power + D_total_power
                        w_count = w_count + 1

                    if days % 7 == 0 and days != 0:

                        W_En = W_Total_Power*24*7/(w_count*d_count)                        # Watt-Hour
                        Weekly_Energy.append(W_En)
                        weeks = weeks + 1
                        W_dates.append(D_dates[days])
                        W_Total_Power = D_total_power
                        w_count = 0

                    D_total_power = i.get_load_power()

                    days = days + 1
                    d_count = 0



                ## Get Average Monthly Power
                if (i.logtime.month == last_logtime.month):

                    M_total_power = M_total_power + i.get_load_power()
                    m_count = m_count + 1

                else:
                    M_En = M_total_power*24*7*4/(m_count)              # Watt-Hour
                    m_d.append(int(time.mktime(last_logtime.timetuple())) * 1000)

                    M_dates.append(last_logtime.month)
                    Y_dates.append(last_logtime.year)

                    M_total_power = i.get_load_power()
                    months = months + 1
                    m_count = 0

                    Monthly_Energy.append(M_En)


            last_logtime = i.logtime

        T_D_Energy = 0
        T_M_Energy = 0
        T_W_Energy = 0
        j = 0
        k = 0
        l = 0


        for j in range (days):

            if j == 0:
                T_D_Energy = int(Daily_Energy[int(j)])
            else:
                T_D_Energy = T_D_Energy + int(Daily_Energy[int(j)])

        Total_Av_Daily_Energy = int(T_D_Energy)/days


        for l in range (weeks):

            if l == 0:
                T_W_Energy = int(Weekly_Energy[int(l)])
            else:
                T_W_Energy = T_W_Energy + int(Weekly_Energy[int(l)])

        Total_Av_Weekly_Energy = int(T_W_Energy)/weeks

        for k in range (months):
            if k == 0:
                T_M_Energy = Monthly_Energy[int(k)]
            else:
                T_M_Energy = T_M_Energy + Monthly_Energy[int(k)]

        Total_Av_Monthly_Energy = int(T_M_Energy)/months






        dbelow = 0
        dabove = 0
        wbelow = 0
        wabove = 0
        mbelow = 0
        mabove = 0

        list1 = []
        list2 = []
        list3 = []



        element = 0
        for element in range (days):

            if Daily_Energy[element] < 0.85*Total_Av_Daily_Energy:
                dbelow = dbelow + 1
                day_date = {'date': D_dates[element], 'Daily_Energy': Daily_Energy[element]}
                list1.append(day_date)
            if Daily_Energy[element] > 1.15*Total_Av_Daily_Energy:
                dabove = dabove + 1
                day_date = {'date': D_dates[element], 'Daily_Energy': Daily_Energy[element]}
                list1.append(day_date)

        element = 0
        for element in range (weeks):

            if Weekly_Energy[element] < 0.85*Total_Av_Weekly_Energy:
                wbelow = wbelow + 1
                day_date = {'date': W_dates[element], 'Average_Weekly_Energy': Weekly_Energy[element]}
                list2.append(day_date)
            if Weekly_Energy[element] > 1.15*Total_Av_Weekly_Energy:
                wabove = wabove + 1
                day_date = {'date': W_dates[element], 'Weekly_Energy': Weekly_Energy[element]}
                list2.append(day_date)

        element = 0
        for element in range (months):

            if Monthly_Energy[element] < 0.85*Total_Av_Monthly_Energy:
                mbelow = mbelow + 1
                month_date = {'date': M_dates[element], 'Year': Y_dates[element], 'Monthly_Energy': Monthly_Energy[element]}
                list3.append(month_date)
            if Monthly_Energy[element] > 1.15*Total_Av_Monthly_Energy:
                mabove = mabove + 1
                month_date = {'date': M_dates[element], 'Year': Y_dates[months-1], 'Monthly_Energy': Monthly_Energy[element]}
                list3.append(month_date)


        Av_D_E = []
        Av_M_E = []
        Av_W_E = []

        Av_D_E = []
        Av_M_E = []
        Av_W_E = []

        U_T_D = []
        L_T_D = []

        U_T_W = []
        L_T_W = []

        U_T_M = []
        L_T_M = []

        for count in range (days):
            Av_D_E.append(int(Daily_Energy[int(count)]))
            L_T_D.append(int(0.85*Total_Av_Daily_Energy))
            U_T_D.append(int(1.15*Total_Av_Daily_Energy))

        for count in range (weeks):
            Av_W_E.append(int(Weekly_Energy[int(count)]))
            L_T_W.append(int(0.85*Total_Av_Weekly_Energy))
            U_T_W.append(int(1.15*Total_Av_Weekly_Energy))

        for count in range (months):
            Av_M_E.append(int(Monthly_Energy[int(count)]))
            L_T_M.append(int(0.85*Total_Av_Monthly_Energy))
            U_T_M.append(int(1.15*Total_Av_Monthly_Energy))


        type = 'lineWithFocusChart'
        chart1 = lineWithFocusChart(name=type, x_is_date=True, x_axis_format="%d %b %Y")
        xdata = D_dates
        ydata = Av_D_E
        extra_serie = {"tooltip": {"y_start": "", "y_end": " V"},
                       "date_format": "%d %b %Y %H:%M:%S"}
        chart1.add_serie(y=ydata, x=xdata, name='Daily Energy(Watt-Hour)', extra=extra_serie)
        chart1.add_serie(y=U_T_D, x=xdata, name='Upper Threshhold', extra=extra_serie)
        chart1.add_serie(y=L_T_D, x=xdata, name='Lower Threshhold', extra=extra_serie)
        chart1.buildcontent()

        chart2 = lineWithFocusChart(name=type, x_is_date=True, x_axis_format="%d %b %Y")
        xdata2 = W_dates
        ydata2 = Av_W_E
        extra_serie = {"tooltip": {"y_start": "", "y_end": " V"},
                       "date_format": "%d %b %Y %H:%M:%S"}
        chart2.add_serie(y=ydata2, x=xdata2, name='Weekly Energy(Watt-Hour)', extra=extra_serie)
        chart2.add_serie(y=U_T_W, x=xdata2, name='Upper Threshhold', extra=extra_serie)
        chart2.add_serie(y=L_T_W, x=xdata2, name='Lower Threshhold', extra=extra_serie)
        chart2.buildcontent()


        chart3 = lineWithFocusChart(name=type, x_is_date=True, x_axis_format="%d %b %Y")
        xdata3 = m_d
        ydata3 = Av_M_E
        extra_serie = {"tooltip": {"y_start": "", "y_end": " V"},
                       "date_format": "%d %b %Y %H:%M:%S"}
        chart3.add_serie(y=ydata3, x=xdata3, name='Monthly Energy(Watt-Hour)', extra=extra_serie)
        chart3.add_serie(y=U_T_M, x=xdata3, name='Upper Threshhold', extra=extra_serie)
        chart3.add_serie(y=L_T_M, x=xdata3, name='Lower Threshhold', extra=extra_serie)
        chart3.buildcontent()



        context['chart1'] = chart1.htmlcontent
        context['chart2'] = chart2.htmlcontent
        context['chart3'] = chart3.htmlcontent



        context['D_av_energy'] = Total_Av_Daily_Energy
        context['W_av_energy'] = Total_Av_Weekly_Energy
        context['M_av_energy'] = Total_Av_Monthly_Energy

        context['dbelow'] = dbelow
        context['dabove'] = dabove
        context['wbelow'] = wbelow
        context['wabove'] = wabove
        context['mbelow'] = mbelow
        context['mabove'] = mabove

        context['table1'] = list1
        context['table2'] = list2
        context['table3'] = list3


        return context
