import csv
from io import StringIO

from django import forms

from uploads.models import Data, DataSet


class UploadForm(forms.Form):
    filename = forms.FileField(max_length=100)
    dsid = forms.IntegerField(widget=forms.HiddenInput())

    def handle_upload(self):
        # Get the dataset data is being appended to
        dataset = DataSet.objects.get(id=self.cleaned_data['dsid'])

        # Get CSV file into string file form
        csvf = StringIO(self.cleaned_data['filename'].read().decode())

        # Put all data into an array
        data = []
        reader = csv.reader(csvf, delimiter=',')
        for row in reader:
            data.append(row)

        # Get rid of first row (headers)
        del data[0]

        # Go through each row in the new dataset and append it
        for row in data:
            self._add_data(dataset, row[0], row[1], row[2], row[3], row[4])

    def _add_data(self, dataset, logtime, b_v, l_i, g_i, g_v):
        Data.objects.get_or_create(
            dataset=dataset,
            logtime=logtime,
            battery_voltage=b_v,
            load_current=l_i,
            gen_current=g_i,
            gen_voltage=g_v)
