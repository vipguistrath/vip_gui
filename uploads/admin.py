from django.contrib import admin

from uploads.models import Data, DataSet


class DataSetAdmin(admin.ModelAdmin):
    list_display = ('name',)
    list_filter = ['updated']


class DataAdmin(admin.ModelAdmin):
    list_filter = ['logtime']

admin.site.register(Data, DataAdmin)
admin.site.register(DataSet, DataSetAdmin)
