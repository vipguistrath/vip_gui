from django.conf.urls import patterns, url

import uploads.views as views

urlpatterns = patterns(
    '',
    url(r'^$', views.IndexView.as_view(), name='index'),
    url(r'^view/(?P<uid>\d+)/$', views.DataSetView.as_view(), name='view'),
    url(r'^view/(?P<uid>\d+)/discharge/$', views.DepthDischargeView.as_view(), name='dod'),
    url(r'^view/(?P<uid>\d+)/level/$', views.BatteryLevelFaultView.as_view(), name='levelfault'),
    url(r'^view/(?P<uid>\d+)/powerfault/$', views.PowerFaultView.as_view(), name='powerfault'),
    url(r'^view/(?P<uid>\d+)/energyfault/$', views.EnergyView.as_view(), name='energy'),
    url(r'^view/(?P<uid>\d+)/powerload/$', views.PowerLoadView.as_view(), name='powerload'),
    url(r'^view/(?P<uid>\d+)/energyload/$', views.EnergyLoadView.as_view(), name='energyload'),
    url(r'^view/(?P<uid>\d+)/upload/$', views.FileUploadView.as_view(), name='upload'),
)
