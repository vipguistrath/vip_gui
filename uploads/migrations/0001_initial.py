# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Upload',
            fields=[
                ('id', models.AutoField(verbose_name='ID', auto_created=True, primary_key=True, serialize=False)),
                ('filename', models.FileField(upload_to='', max_length=50)),
                ('created', models.DateTimeField(verbose_name='date uploaded')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
