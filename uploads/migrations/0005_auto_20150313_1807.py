# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('uploads', '0004_auto_20150306_0937'),
    ]

    operations = [
        migrations.CreateModel(
            name='Data',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('logtime', models.DateTimeField()),
                ('gen_voltage', models.DecimalField(decimal_places=2, max_digits=4)),
                ('gen_current', models.DecimalField(decimal_places=2, max_digits=4)),
                ('load_voltage', models.DecimalField(decimal_places=2, max_digits=4)),
                ('load_current', models.DecimalField(decimal_places=2, max_digits=4)),
            ],
            options={
                'verbose_name_plural': 'data',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='DataSet',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=50)),
                ('updated', models.DateTimeField(auto_now=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.RemoveField(
            model_name='gambiadata',
            name='upload',
        ),
        migrations.DeleteModel(
            name='GambiaData',
        ),
        migrations.DeleteModel(
            name='Upload',
        ),
        migrations.AddField(
            model_name='data',
            name='dataset',
            field=models.ForeignKey(to='uploads.DataSet'),
            preserve_default=True,
        ),
    ]
