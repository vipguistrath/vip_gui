# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('uploads', '0003_auto_20150220_2334'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='gambiadata',
            options={'ordering': ['logtime']},
        ),
        migrations.AlterField(
            model_name='gambiadata',
            name='gprs',
            field=models.BooleanField(default=False),
            preserve_default=True,
        ),
    ]
