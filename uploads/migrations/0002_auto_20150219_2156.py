# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('uploads', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Data',
            fields=[
                ('id', models.AutoField(primary_key=True, auto_created=True, verbose_name='ID', serialize=False)),
                ('logtime', models.DateTimeField()),
                ('sensor_0', models.DecimalField(decimal_places=2, max_digits=4, blank=True, null=True)),
                ('sensor_1', models.DecimalField(decimal_places=2, max_digits=4, blank=True, null=True)),
                ('sensor_2', models.DecimalField(decimal_places=2, max_digits=4, blank=True, null=True)),
                ('sensor_3', models.DecimalField(decimal_places=2, max_digits=4, blank=True, null=True)),
                ('sensor_4', models.DecimalField(decimal_places=2, max_digits=4, blank=True, null=True)),
                ('sensor_5', models.DecimalField(decimal_places=2, max_digits=4, blank=True, null=True)),
                ('sensor_6', models.DecimalField(decimal_places=2, max_digits=4, blank=True, null=True)),
                ('sensor_7', models.DecimalField(decimal_places=2, max_digits=4, blank=True, null=True)),
                ('sensor_8', models.DecimalField(decimal_places=2, max_digits=4, blank=True, null=True)),
                ('sensor_9', models.DecimalField(decimal_places=2, max_digits=4, blank=True, null=True)),
                ('sensor_10', models.DecimalField(decimal_places=2, max_digits=4, blank=True, null=True)),
                ('sensor_11', models.DecimalField(decimal_places=2, max_digits=4, blank=True, null=True)),
                ('status_0', models.PositiveSmallIntegerField(blank=True, null=True)),
                ('status_1', models.PositiveSmallIntegerField(blank=True, null=True)),
                ('upload', models.ForeignKey(to='uploads.Upload')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='upload',
            name='sensor_0',
            field=models.CharField(null=True, blank=True, max_length=20),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='upload',
            name='sensor_1',
            field=models.CharField(null=True, blank=True, max_length=20),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='upload',
            name='sensor_10',
            field=models.CharField(null=True, blank=True, max_length=20),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='upload',
            name='sensor_11',
            field=models.CharField(null=True, blank=True, max_length=20),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='upload',
            name='sensor_2',
            field=models.CharField(null=True, blank=True, max_length=20),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='upload',
            name='sensor_3',
            field=models.CharField(null=True, blank=True, max_length=20),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='upload',
            name='sensor_4',
            field=models.CharField(null=True, blank=True, max_length=20),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='upload',
            name='sensor_5',
            field=models.CharField(null=True, blank=True, max_length=20),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='upload',
            name='sensor_6',
            field=models.CharField(null=True, blank=True, max_length=20),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='upload',
            name='sensor_7',
            field=models.CharField(null=True, blank=True, max_length=20),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='upload',
            name='sensor_8',
            field=models.CharField(null=True, blank=True, max_length=20),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='upload',
            name='sensor_9',
            field=models.CharField(null=True, blank=True, max_length=20),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='upload',
            name='status_0',
            field=models.CharField(null=True, blank=True, max_length=20),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='upload',
            name='status_1',
            field=models.CharField(null=True, blank=True, max_length=20),
            preserve_default=True,
        ),
    ]
