# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('uploads', '0005_auto_20150313_1807'),
    ]

    operations = [
        migrations.RenameField(
            model_name='data',
            old_name='gen_voltage',
            new_name='battery_voltage',
        ),
        migrations.RenameField(
            model_name='data',
            old_name='load_voltage',
            new_name='gen_voltage',
        ),

    ]
