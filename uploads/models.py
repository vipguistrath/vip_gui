from django.db import models


class DataSet(models.Model):
    """This class is the model for each individual dataset."""
    name = models.CharField(max_length=50)
    updated = models.DateTimeField(auto_now=True)

    def __str__(self):
        return str(self.name)


class Data(models.Model):
    """This class is the model for each row of data from a given dataset."""
    dataset = models.ForeignKey(DataSet)
    logtime = models.DateTimeField()
    battery_voltage = models.DecimalField(max_digits=4, decimal_places=2)
    gen_current = models.DecimalField(max_digits=4, decimal_places=2)
    gen_voltage = models.DecimalField(max_digits=4, decimal_places=2)
    load_current = models.DecimalField(max_digits=4, decimal_places=2)

    class Meta:
        ordering = ['logtime']

    def load_is_above(self, val):
        """This method returns True if the battery is above a certain value."""
        return self.battery_voltage > val

    def load_is_below(self, val):
        """This method returns True if the battery is below a certain value."""
        return self.battery_voltage < val

    def get_gen_power(self):
        return self.gen_voltage * self.gen_current

    def get_load_power(self):
        return self.battery_voltage * self.load_current

    def __str__(self):
        return str(self.logtime)

    class Meta:
        verbose_name_plural = "data"
